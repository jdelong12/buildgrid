# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest
import platform
import multiprocessing


@pytest.fixture(scope="session", autouse=True)
def set_multiprocessing_fork_method(request):
    """
    This fixture is run once before any other tests are run.

    Set the multiprocessing start method.
    Python3.8, on OSX introduced a new start method "spawn". This causes issues when pickling certain objects.
    Setting the start method to "fork", the default in previous interpreters, resolves these issues.
    """
    if platform.system() == "Darwin":
        multiprocessing.set_start_method("fork")
