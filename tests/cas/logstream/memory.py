# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from unittest import mock
import pytest

from buildgrid._exceptions import StreamAlreadyExistsError
from buildgrid.server.cas.logstream.stream_storage.stream_storage_abc import StreamStorageABC, StreamHandle, StreamLength, StreamChunk
from buildgrid.server.cas.logstream.stream_storage.memory import MemoryStreamStorage

@pytest.fixture()
def memory_stream_storage():
    yield MemoryStreamStorage(max_streams_count=10)


def test_create_stream(memory_stream_storage):
    sh = StreamHandle._create_new(parent='parent', prefix='prefix')
    memory_stream_storage._create_stream(sh)


def test_create_stream_duplicate_throws(memory_stream_storage):
    sh = StreamHandle._create_new(parent='parent', prefix='prefix')
    memory_stream_storage._create_stream(sh)
    with pytest.raises(StreamAlreadyExistsError):
        memory_stream_storage._create_stream(sh)


def test_lru_cleanup(memory_stream_storage):
    initial_stream_handles = []
    assert len(memory_stream_storage._streams) == 0
    for n in range(memory_stream_storage._max_stream_count):
        sh = StreamHandle._create_new(parent='parent', prefix='prefix')
        initial_stream_handles.append(sh)
        memory_stream_storage._create_stream(sh)
        # We should have one more stream now
        assert len(memory_stream_storage._streams) == n + 1

    # We should've reached the maximum streams we are allowed to keep in memory
    assert len(memory_stream_storage._streams) == memory_stream_storage._max_stream_count

    # Adding one more logstream should trigger cleanup
    sh = StreamHandle._create_new(parent='parent', prefix='prefix')
    memory_stream_storage._create_stream(sh)

    # Make sure cleanup has happened
    assert len(memory_stream_storage._streams) < memory_stream_storage._max_stream_count

    # Make sure the new stream is actually in memory
    assert sh.name in memory_stream_storage._streams


def test_lru_cleanup_with_readers(memory_stream_storage):
    initial_stream_handles = []
    assert len(memory_stream_storage._streams) == 0
    for n in range(memory_stream_storage._max_stream_count):
        sh = StreamHandle._create_new(parent='parent', prefix='prefix')
        initial_stream_handles.append(sh)
        memory_stream_storage._create_stream(sh)
        # Increament the number of clients streaming this logstream
        # to force keep it in memory
        memory_stream_storage.new_client_streaming(sh.name)
        # We should have one more stream now
        assert len(memory_stream_storage._streams) == n + 1

    # We should've reached the maximum streams we are allowed to keep in memory
    assert len(memory_stream_storage._streams) == memory_stream_storage._max_stream_count

    # Adding one more logstream should trigger cleanup but since there are readers
    # we won't actually remove the current streams
    sh_new = StreamHandle._create_new(parent='parent', prefix='prefix')
    memory_stream_storage._create_stream(sh_new)
    assert len(memory_stream_storage._streams) == memory_stream_storage._max_stream_count +1

    # Now go remove the readers of the odd-numbered-streams
    no_more_readers_streams = []
    for n in range(1, len(initial_stream_handles), 2):
        memory_stream_storage.streaming_client_left(initial_stream_handles[n].name)
        no_more_readers_streams.append(initial_stream_handles[n])

    # Adding one more logstream should trigger cleanup of all unused
    # logstreams at this point
    sh_last = StreamHandle._create_new(parent='parent', prefix='prefix')
    memory_stream_storage._create_stream(sh_last)

    # Make sure cleanup has happened
    assert len(memory_stream_storage._streams) < memory_stream_storage._max_stream_count

    # Make sure the new streams are actually in memory
    assert sh_new.name in memory_stream_storage._streams
    assert sh_last.name in memory_stream_storage._streams

    # Make sure the cleanup removed the streams with 0 readers
    # but kept the ones with readers
    for stream_handle in no_more_readers_streams:
        assert stream_handle.name not in memory_stream_storage._streams
    for stream_handle in initial_stream_handles:
        if stream_handle not in no_more_readers_streams:
            assert stream_handle.name in memory_stream_storage._streams

