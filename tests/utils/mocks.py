# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import RequestMetadata


MOCK_TOOL_NAME = 'bgd-test-suite'
MOCK_TOOL_VERSION = '0.0.0'
MOCK_INVOCATION_ID = 'test-invocation'
MOCK_CORRELATED_INVOCATIONS_ID = 'test-correlation'


def mock_extract_request_metadata(context) -> RequestMetadata:
    metadata = RequestMetadata()
    metadata.tool_invocation_id = MOCK_INVOCATION_ID
    metadata.correlated_invocations_id = MOCK_CORRELATED_INVOCATIONS_ID
    metadata.tool_details.tool_name = MOCK_TOOL_NAME
    metadata.tool_details.tool_version = MOCK_TOOL_VERSION
    return metadata